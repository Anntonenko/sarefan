package com.nikita.sarefan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SarefanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SarefanApplication.class, args);
	}

}
