package com.nikita.sarefan.entitys;

public final class Views {
    public interface Id {}

    public interface IdName extends Id {}

    public interface FullMessage extends Id {}
}
