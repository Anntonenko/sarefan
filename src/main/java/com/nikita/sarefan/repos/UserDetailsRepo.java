package com.nikita.sarefan.repos;

import com.nikita.sarefan.entitys.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepo extends JpaRepository<User, String> {
}

