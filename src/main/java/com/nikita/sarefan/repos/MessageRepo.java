package com.nikita.sarefan.repos;

import com.nikita.sarefan.entitys.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepo extends JpaRepository<Message, Long> {
}
